import {TestBed, getTestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';


import {CommunicationService} from './communication.service';
import * as config from "./configuration";
import {IParking} from "../models/parking.model";


describe('CommunicationService', () => {

  let injector: TestBed;
  let service: CommunicationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [CommunicationService]
    });

    injector = getTestBed();
    service = injector.get(CommunicationService);
    httpMock = injector.get(HttpTestingController);

  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });


  describe('getParkingData()', () => {
    it('should return data', () => {

      const testOutput = [
        {
          'discriminator': '',
          'address': {
            'street': '',
            'house': '',
            'city': ''
          },
          'spacesCount': 0,
          'availableSpacesCount': 0,
          'id': '',
          'name': '',
          'description': '',
          'location': {
            'latitude': 0,
            'longitude': 0
          }
        }
      ];

      service.getParkingData().subscribe((parkingData: IParking[]) => {
        if(parkingData) {
          expect(parkingData.length).toBe(1);
          expect(JSON.stringify(parkingData)).toEqual(JSON.stringify(testOutput));
        }
      });

      const req = httpMock.expectOne(`${config.API_URL}/map?objectType=PARKING`);
      expect(req.request.method).toBe("GET");
      req.flush(testOutput);
    });
  });

});
