import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from "rxjs/operators";

import *  as config from './configuration';
import {IParkingResponse, IParking} from '../models/parking.model';

@Injectable({
  providedIn: 'root'
})
export class CommunicationService {

  /**
   * constructor
   * @param http
   */
  constructor(private http: HttpClient) {
  }

  /**
   * Fetch parking data form API
   * return {Observable<IParking[]>}
   */
  public getParkingData(): Observable<IParking[]> {
    return this.http.get<IParkingResponse>(`${config.API_URL}/map?objectType=PARKING`).pipe(
      map((apiData: IParkingResponse) => apiData.objects)
    );
  }
}
