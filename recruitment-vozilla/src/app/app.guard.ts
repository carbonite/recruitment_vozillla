import {Injectable} from '@angular/core';
import {CanActivate} from '@angular/router';

import {catchError, switchMap, tap, filter, take} from 'rxjs/operators';
import {Observable, of} from 'rxjs';

import {select, Store} from '@ngrx/store';
import * as appStore from '@store/index';

@Injectable()
export class ParkingGuard implements CanActivate {
  constructor(
    private store: Store<appStore.StoreNodes>,
  ) {
  }

  canActivate(): Observable<boolean> {
    return this.loadParkingList().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    );
  }

  loadParkingList(): Observable<boolean> {
     return this.store.pipe(select(appStore.selectorParkingLoaded),
     tap((loaded: boolean) => {
         if (!loaded) {
           this.store.dispatch(new appStore.LoadParkingList());
         }
       }),
      filter((loaded) => loaded),
      take(1)
   );
  }
}

