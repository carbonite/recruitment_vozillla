export interface IParkingResponse {
  'objects': IParking[];
}

export interface IParking {
  'discriminator': string;
  'address': IParkingAddress;
  'spacesCount': number;
  'availableSpacesCount': number;
  'id': string;
  'name': string;
  'description': string;
  'location': IParkingGeo;
  'chargers'?: string[];
  'metadata'?: string;
  'distance'? :number;
}

interface IParkingAddress {
  'street': string;
  'house': string;
  'city': string;
}

interface IParkingGeo {
  'latitude': number;
  'longitude': number;
}
