import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {ParkingComponent} from './pages/parking/parking.component';
import {ErrorComponent} from './pages/error/error.component';
import {ParkingGuard} from "./app.guard";

const routes: Routes = [
  {
    path: '',
    redirectTo: 'parking',
    pathMatch: 'full'
  },
  {
    path: 'parking',
    component: ParkingComponent,
    canActivate: [ParkingGuard]
  },
  {
    path: 'error',
    component: ErrorComponent
  },
  {
    path: '**',
    redirectTo: 'error'
  }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
