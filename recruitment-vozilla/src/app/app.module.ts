import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {CommunicationService} from '@services/communication.service';

import {environment} from '@environment';

import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
import {parkingListFeatureName} from '@store/store.config';
import {storeReducers} from '@store/reducers';
import {effects} from '@store/effects';

import {ParkingComponent} from './pages/parking/parking.component';
import {ErrorComponent} from './pages/error/error.component';
import {ParkingGuard} from "./app.guard";

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatCardModule} from '@angular/material/card';
import {MatTableModule} from '@angular/material/table';

const development = environment.production
  ? []
  : [StoreDevtoolsModule.instrument()];


@NgModule({
  declarations: [
    AppComponent,
    ParkingComponent,
    ErrorComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatToolbarModule,
    MatIconModule,
    MatCardModule,
    MatTableModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    StoreModule.forRoot({}),
    StoreModule.forFeature(parkingListFeatureName, storeReducers),
    EffectsModule.forRoot(effects),
    ...development
  ],
  providers: [CommunicationService, ParkingGuard],
  bootstrap: [AppComponent]
})
export class AppModule {
}
