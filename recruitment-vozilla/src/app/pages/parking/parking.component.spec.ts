import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {MatCardModule, MatIconModule, MatTableModule, MatToolbarModule} from '@angular/material';


import {StoreModule, Store} from '@ngrx/store';

import * as reducers from '../../store/reducers';
import * as actions from '../../store/actions';

import {IParking} from '../../models/parking.model';
import {parkingListFeatureName} from '../../store/store.config';

import {ParkingComponent} from './parking.component';


describe('ParkingComponent', () => {
  let component: ParkingComponent;
  let fixture: ComponentFixture<ParkingComponent>;
  let store: Store<reducers.StoreNodes>;

  const payload: IParking[] = [
    {
      'discriminator': '',
      'address': {
        'street': '',
        'house': '',
        'city': ''
      },
      'spacesCount': 0,
      'availableSpacesCount': 0,
      'id': '',
      'name': '',
      'description': '',
      'location': {
        'latitude': 0,
        'longitude': 0
      }
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        MatTableModule,
        StoreModule.forRoot({}),
        StoreModule.forFeature(parkingListFeatureName, reducers.storeReducers)
      ],
      declarations: [ParkingComponent]
    })
      .compileComponents();

    spyOn(navigator.geolocation, "getCurrentPosition").and.callFake(function () {
      arguments[0]({coords: {latitude: 0, longitude: 0}})
    });

    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit geolocation$ with correct location', () => {
    let geo;
    component.geolocation$.subscribe(result => (geo = result));
    component.ngOnInit();
    expect(geo).toEqual({"lat": 0, "long": 0});
  });


  it('should return object with distance', () => {
    let result;

    component.parkingList$.subscribe(value => (result = value));
    store.dispatch(new actions.LoadParkingListSuccess(payload));
    component.ngOnInit();
    expect(JSON.stringify(result)).toEqual(JSON.stringify([{
      discriminator: '',
      address: {street: '', house: '', city: ''},
      spacesCount: 0,
      availableSpacesCount: 0,
      id: '',
      name: '',
      description: '',
      location: {latitude: 0, longitude: 0},
      distance: 0
    }]));
  });

});
