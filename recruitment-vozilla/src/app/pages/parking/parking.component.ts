import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";

import {select, Store} from '@ngrx/store';
import * as appStore from '../../store/index';

import {Observable, Subject} from 'rxjs';
import {map, switchMap} from 'rxjs/operators';

import {IParking} from '../../models/parking.model';

export interface IGeoLocation {
  lat: number;
  long: number;
}

@Component({
  selector: 'app-parking',
  templateUrl: './parking.component.html',
  styleUrls: ['./parking.component.scss']
})
export class ParkingComponent implements OnInit {

  parkingList$: Observable<IParking[]>;
  geolocation$ = new Subject<IGeoLocation | null>();
  parkingTableHeader: String[] = ['name', 'address', 'spacesCount', 'distance'];

  /**
   * constructor
   * @param store
   * @param router
   */
  constructor(private store: Store<appStore.StoreNodes>,
              private router: Router) {
  }

  /**
   * on component initialisation
   */
  ngOnInit(): void {
    this.parkingList$ = this.geolocation$.asObservable().pipe(
      switchMap((geolocation: IGeoLocation | null) => {
        return this.store.pipe(
          select(appStore.selectorParkingList),
          map((parkingList: IParking[]) => {
            return (parkingList || []).map((parking: IParking) => {
              parking.distance = geolocation ? this._calculateDistance([
                {lat: geolocation.lat, long: geolocation.long},
                {lat: parking.location.latitude, long: parking.location.longitude}
              ]) : 0;
              return parking;
            }).sort((parkingA: IParking, parkingB: IParking) => parkingA.distance - parkingB.distance)
          })
        )
      })
    );
    // setup default position at next tick
    setTimeout(() => this.geolocation$.next(), 0);
    this._getUserGeo();
  }

  /**
   * Method which get user's location from webbrowser navigator.
   * In case of success emit coordinates in observable
   * @private
   */
  private _getUserGeo(): void {
    if (window.navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position: any) => {
          const geolocation: IGeoLocation = {
            lat: position.coords.latitude,
            long: position.coords.longitude
          };
          this.geolocation$.next(geolocation);
        },
        () => this.router.navigate(['error']));
    } else {
      this.router.navigate(['error'])
    }
  }

  /**
   * method which calculate distance in km between two coordinates,
   * based on: https://www.geodatasource.com/developers/javascript
   * @param geo {IGeoLocation[]}
   * @return {number}
   * @private
   */
  private _calculateDistance(geo: IGeoLocation[]): number {
    const [geoA, geoB] = geo;
    const {lat: latA, long: lonA} = geoA;
    const {lat: latB, long: lonB} = geoB;
    const radlatA = Math.PI * latA / 180;
    const radlatB = Math.PI * latB / 180;
    const theta = lonA - lonB;
    const radtheta = Math.PI * theta / 180;
    let dist = Math.sin(radlatA) * Math.sin(radlatB) + Math.cos(radlatA) * Math.cos(radlatB) * Math.cos(radtheta);
    dist = dist > 1 ? 1 : dist;
    dist = Math.acos(dist);
    dist = dist * 180 / Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 1.609344;
    return dist;
  }

}
