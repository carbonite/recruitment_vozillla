import {HttpClientTestingModule} from '@angular/common/http/testing';
import {RouterTestingModule} from '@angular/router/testing';
import {TestBed} from '@angular/core/testing';

import {Actions} from '@ngrx/effects';

import {hot, cold} from 'jasmine-marbles';
import {Observable, of, empty} from 'rxjs';

import {CommunicationService} from '../../services/communication.service';
import * as pEffects from './parking.list.effects';
import * as actions from '../actions/parking.list.actions';
import {IParking} from '../../models/parking.model';

export class TestActions extends Actions {
  constructor() {
    super(empty());
  }

  set stream(source: Observable<any>) {
    this.source = source;
  }
}

export function getActions() {
  return new TestActions();
}

describe('Parking List Effects', () => {
  let actions$: TestActions;
  let service: CommunicationService;
  let effects: pEffects.ParkingListEffects;

  const payload: IParking[] = [
    {
      'discriminator': '',
      'address': {
        'street': '',
        'house': '',
        'city': ''
      },
      'spacesCount': 0,
      'availableSpacesCount': 0,
      'id': '',
      'name': '',
      'description': '',
      'location': {
        'latitude': 0,
        'longitude': 0
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule],
      providers: [
        CommunicationService,
        pEffects.ParkingListEffects,
        {provide: Actions, useFactory: getActions},
      ],
    });

    actions$ = TestBed.get(Actions);
    service = TestBed.get(CommunicationService);
    effects = TestBed.get(pEffects.ParkingListEffects);

    spyOn(service, 'getParkingData').and.returnValue(of(payload));
  });

  describe('loadParkingList$', () => {
    it('should return a collection from LoadParkingListSuccess', () => {
      const action = new actions.LoadParkingList();
      const completion = new actions.LoadParkingListSuccess(payload);

      actions$.stream = hot('-a', {a: action});
      const expected = cold('-b', {b: completion});

      expect(effects.loadParkingList$).toBeObservable(expected);
    });
  });
});
