import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

import {Actions, Effect, ofType} from '@ngrx/effects';
import * as actions from '../actions/parking.list.actions';

import {of} from 'rxjs/';
import {catchError, map, switchMap} from 'rxjs/operators';

import {CommunicationService} from '../../services/communication.service';

import {IParking} from '../../models/parking.model';

@Injectable()
export class ParkingListEffects {

  /**
   * effect which transform loading parking list into saved data in store
   */
  @Effect()
  loadParkingList$ = this.actions$.pipe(
    ofType(actions.LOAD_PARKING_LIST),
    switchMap(() => {
      return this.communicationService.getParkingData().pipe(
        map((parkingData: IParking[]) => new actions.LoadParkingListSuccess(parkingData)),
        catchError(error => {
          this.router.navigate(['error']);
          return of(new actions.LoadParkingListFail(error))
        })
      );
    })
  );

  /**
   * constructor
   * @param actions$
   * @param communicationService
   * @param router
   */
  constructor(private actions$: Actions,
              private communicationService: CommunicationService,
              private router: Router) {
  }
}
