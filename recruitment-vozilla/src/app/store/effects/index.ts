import { ParkingListEffects } from './parking.list.effects';

export const effects: any [] = [
  ParkingListEffects
];

export * from './parking.list.effects';
