import { ActionReducerMap, createFeatureSelector } from '@ngrx/store';
import { parkingListFeatureName } from '../store.config';
import * as plReducers from './parking.list.reducers';

export interface StoreNodes {
  parking: plReducers.IParkingListState;
}

export const storeReducers: ActionReducerMap<StoreNodes> = {
  parking: plReducers.reducer,
};

export const stateStore =
  createFeatureSelector<StoreNodes>(parkingListFeatureName);
