import { IParking } from '../../models/parking.model';
import * as actions from '../actions/parking.list.actions';

export interface IParkingListState {
  list: IParking[] | null;
  loaded: boolean;
  loading: boolean;
}

export const initialState: IParkingListState = {
  list: null,
  loaded: false,
  loading: false
};

export function reducer(
  state: IParkingListState = initialState,
  action: actions.ParkingListActions
) {
  switch (action.type) {
    case actions.LOAD_PARKING_LIST: {
      return { ...state, loading: true };
    }

    case actions.LOAD_PARKING_LIST_FAIL: {
      return { ...state, loading: false, loaded: false };
    }

    case actions.LOAD_PARKING_LIST_SUCCESS: {
      return {
        ...state,
        list: (<actions.LoadParkingListSuccess>action).payload,
        loading: false,
        loaded: true
      };
    }
  }
  return state;
}
