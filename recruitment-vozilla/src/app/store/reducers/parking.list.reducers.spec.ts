import * as reducers from './parking.list.reducers';
import * as actions from '../actions/parking.list.actions';
import {IParking} from '../../models/parking.model';
import {LoadParkingList} from '../actions';

describe('Parking List Reducers', () => {
  describe('undefined action', () => {
    it('should return the default state', () => {
      const {initialState} = reducers;
      const action = {} as any;
      const state = reducers.reducer(undefined, action);

      expect(state).toBe(initialState);
    });
  });

  describe('LOAD_PARKING_LIST action', () => {
    it('should set loading to true', () => {
      const {initialState} = reducers;
      const action: LoadParkingList = new actions.LoadParkingList();
      const state: any = reducers.reducer(initialState, action);

      expect(state.loading).toEqual(true);
      expect(state.loaded).toEqual(false);
      expect(state.list).toEqual(null);
    });
  });

  describe('LOAD_PARKING_LIST_SUCCESS action', () => {
    it('should set parking list', () => {
      const payload: IParking[] = [
        {
          'discriminator': '',
          'address': {
            'street': '',
            'house': '',
            'city': ''
          },
          'spacesCount': 0,
          'availableSpacesCount': 0,
          'id': '',
          'name': '',
          'description': '',
          'location': {
            'latitude': 0,
            'longitude': 0
          }
        }
      ];
      const {initialState} = reducers;
      const action = new actions.LoadParkingListSuccess(payload);
      const state: any = reducers.reducer(initialState, action);

      expect(state.loaded).toEqual(true);
      expect(state.loading).toEqual(false);
      expect(state.list).toEqual(payload);
    });
  });

  describe('LOAD_PARKING_LIST_FAIL action', () => {
    it('should return the initial state', () => {
      const {initialState} = reducers;
      const action = new actions.LoadParkingListFail({});
      const state: any = reducers.reducer(initialState, action);

      expect(state).toEqual(initialState);
    });
  });

});
