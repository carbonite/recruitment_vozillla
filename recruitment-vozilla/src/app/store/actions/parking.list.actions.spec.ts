import {
  LOAD_PARKING_LIST,
  LOAD_PARKING_LIST_FAIL,
  LOAD_PARKING_LIST_SUCCESS,
  LoadParkingList,
  LoadParkingListFail, LoadParkingListSuccess
} from "./parking.list.actions";
import {IParking} from "../../models/parking.model";

describe('LoadParkingList', () => {
  it('should create an action', () => {
    const action = new LoadParkingList();
    expect(action.type).toEqual(LOAD_PARKING_LIST);
  });
});

describe('LoadParkingListFail', () => {
  it('should create an action', () => {
    const payload: string = 'error';
    const action = new LoadParkingListFail(payload);
    expect({...action}).toEqual({type: LOAD_PARKING_LIST_FAIL, payload});
  });
});

describe('LoadParkingListSuccess', () => {
  it('should create an action', () => {
    const payload: IParking[] = [
      {
        'discriminator': '',
        'address': {
          'street': '',
          'house': '',
          'city': ''
        },
        'spacesCount': 0,
        'availableSpacesCount': 0,
        'id': '',
        'name': '',
        'description': '',
        'location': {
          'latitude': 0,
          'longitude': 0
        }
      }
    ];

    const action = new LoadParkingListSuccess(payload);
    expect({...action}).toEqual({
      type: LOAD_PARKING_LIST_SUCCESS,
      payload,
    });
  });
});
