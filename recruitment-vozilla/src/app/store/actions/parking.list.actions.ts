import { Action } from '@ngrx/store';
import { parkingListFeatureName } from '../store.config';
import {IParking} from '../../models/parking.model';

export const LOAD_PARKING_LIST = `[${parkingListFeatureName}] Load`;
export const LOAD_PARKING_LIST_FAIL = `[${parkingListFeatureName}] Load Fail`;
export const LOAD_PARKING_LIST_SUCCESS = `[${parkingListFeatureName}] Load Success`;

export class LoadParkingList implements Action {
  readonly type = LOAD_PARKING_LIST;
}

export class LoadParkingListFail implements Action {
  readonly type = LOAD_PARKING_LIST_FAIL;

  constructor(public payload: any) {}
}

export class LoadParkingListSuccess implements Action {
  readonly type = LOAD_PARKING_LIST_SUCCESS;

  constructor(public payload: IParking[]) {}
}

export type ParkingListActions =
  LoadParkingList
  | LoadParkingListFail
  | LoadParkingListSuccess;
