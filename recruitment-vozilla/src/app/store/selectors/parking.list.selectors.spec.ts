import {TestBed} from '@angular/core/testing';
import {StoreModule, Store} from '@ngrx/store';

import * as reducers from '../reducers';
import * as actions from '../actions';
import * as selectors from '../selectors/parking.list.selectors';

import {IParking} from '../../models/parking.model';
import {parkingListFeatureName} from '../store.config';

describe('Parking List Selectors', () => {
  let store: Store<reducers.StoreNodes>;

  const payload: IParking[] = [
    {
      'discriminator': '',
      'address': {
        'street': '',
        'house': '',
        'city': ''
      },
      'spacesCount': 0,
      'availableSpacesCount': 0,
      'id': '',
      'name': '',
      'description': '',
      'location': {
        'latitude': 0,
        'longitude': 0
      }
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        StoreModule.forRoot({}),
        StoreModule.forFeature(parkingListFeatureName, reducers.storeReducers),
      ],
    });

    store = TestBed.get(Store);

    spyOn(store, 'dispatch').and.callThrough();
  });

  describe('selectorParkingLoaded', () => {
    it('should return loaded as true', () => {
      let result;

      store
        .select(selectors.selectorParkingLoaded)
        .subscribe(value => (result = value));

      expect(result).toEqual(false);

      store.dispatch(new actions.LoadParkingListSuccess(payload));

      expect(result).toEqual(true);
    });
  });

  describe('selectorParkingList', () => {
    it('should return list', () => {
      let result;

      store
        .select(selectors.selectorParkingList)
        .subscribe(value => (result = value));

      expect(result).toEqual(null);

      store.dispatch(new actions.LoadParkingListSuccess(payload));

      expect(result).toEqual(payload);
    });
  });
});
