import { createSelector } from '@ngrx/store';
import * as indexReducers from '../reducers';

export const selectorParkingList = createSelector(indexReducers.stateStore,
  (state: indexReducers.StoreNodes) => state && state.parking && state.parking.list
);

export const selectorParkingLoaded = createSelector(indexReducers.stateStore,
  (state: indexReducers.StoreNodes) => state && state.parking && state.parking.loaded
);
